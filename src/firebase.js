import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyB8H6zhNCtmB6Bor-g5dbxEn2vtCaeE2bM",
    authDomain: "login-facebook-3a9de.firebaseapp.com",
    projectId: "login-facebook-3a9de",
    storageBucket: "login-facebook-3a9de.appspot.com",
    messagingSenderId: "630274580392",
    appId: "1:630274580392:web:e0547f2cfd6b7e83cf879d"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;